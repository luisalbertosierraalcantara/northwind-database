## Porting northwind database from Mircrosoft SQL Server to Oracle,Sqlite,Firebird,MySQL and PostgreSQL

**Northwind Traders** is a database sample that is shipped along with the Microsoft Access application. The **Northwind database** is available under a **Microsoft Public License**.

This project contain the northwind databases for SQLServer, MariaDB/MySQL,Oracle,PostgreSQL,SQLite3 and Firebird 3

With **Tables**,**Views**,**User Defined Function** and **Stored Procedure**.

## Entity-Relationship Diagram
<img src="https://gitlab.com/luisalbertosierraalcantara/northwind-database/-/raw/main/Northwind.png" />

## SQL Scripts
* MySQL
* PostgreSQL
* SQL Server
* SQLite
* Firebird
* Oracle

## Tables

* Categories
* Customers
* Employees
* OrderDetails
* Orders
* Products
* Shippers
* Suppliers

## View
* alphabetical list of product
* category sales for 1997
* current product list
* customer and suppliers by city
* invoices
* orders qry
* order details extended
* order subtotals
* products above average price
* products by category
* product sales for 1997
* quarterly orders
* sales by category
* sales totals by amount
* summary of sales by quarter
* summary of sales by year

## Functions
* DateOnly
* MyRound

## Precedure
* LookByFName
* Employees Insert
* Employees SelectAll
* Employees SelectRow
* Employees Update
* employees rank
* employees rollup
* employees rownum

## This repository contains the article files:
<a href="https://interesting-homemade-projects.blogspot.com/2024/05/porting-northwind-database-to.html" target="_blank">https://interesting-homemade-projects.blogspot.com/2024/05/porting-northwind-database-to.html</a>