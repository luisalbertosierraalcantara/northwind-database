-- Copyright Microsoft, Inc. 1994 - 2000
-- All Rights Reserved.
--
-- Create database Northwind SQLServer 2012 and Up
-- Modify by Luis A. Sierra
-- Blog: https://interesting-homemade-projects.blogspot.com
--
-- TESTED in Database Management ** SQL Management Studio 2012 **
-- https://www.microsoft.com/en-us/download/details.aspx?id=43351
--

CREATE DATABASE Northwind
GO

USE Northwind

-- Table Categories
CREATE TABLE Categories (
  CategoryID int IDENTITY(1, 1) NOT NULL,
  CategoryName varchar(15)  NOT NULL,
  Description text  NULL,
  Picture image NULL,
  CONSTRAINT PK_Categories PRIMARY KEY (CategoryID)
)
GO

-- Table Customers
CREATE TABLE Customers (
  CustomerID char(5)  NOT NULL,
  CompanyName varchar(40)  NOT NULL,
  ContactName varchar(30)  NULL,
  ContactTitle varchar(30)  NULL,
  Address varchar(60)  NULL,
  City varchar(15)  NULL,
  Region varchar(15)  NULL,
  PostalCode varchar(10)  NULL,
  Country varchar(15)  NULL,
  Phone varchar(24)  NULL,
  Fax varchar(24)  NULL,
  CONSTRAINT PK_Customers PRIMARY KEY (CustomerID)
)
GO

-- Table CustomerCustomerDemo
CREATE TABLE CustomerCustomerDemo(
	CustomerID char(5) NOT NULL,
	CustomerTypeID char(10) NOT NULL,
    CONSTRAINT PK_CustomerCustomerDemo PRIMARY KEY (CustomerID)
)
 
-- Table CustomerDemographics
CREATE TABLE CustomerDemographics (
  CustomerTypeID char(10)  NOT NULL,
  CustomerDesc text  NULL,
  CONSTRAINT PK_CustomerDemographics PRIMARY KEY (CustomerTypeID)
)
GO

-- Table Employees
CREATE TABLE Employees (
  EmployeeID int IDENTITY(1, 1) NOT NULL,
  LastName varchar(20)  NOT NULL,
  FirstName varchar(10)  NOT NULL,
  Title varchar(30)  NULL,
  TitleOfCourtesy varchar(25)  NULL,
  BirthDate datetime NULL,
  HireDate datetime NULL,
  Address varchar(60)  NULL,
  City varchar(15)  NULL,
  Region varchar(15)  NULL,
  PostalCode varchar(10)  NULL,
  Country varchar(15)  NULL,
  HomePhone varchar(24)  NULL,
  Extension varchar(4)  NULL,
  Photo image NULL,
  Notes text  NULL,
  ReportsTo int NULL,
  PhotoPath varchar(255)  NULL,
  Salary decimal(18, 2) NULL,
  CONSTRAINT PK_Employees PRIMARY KEY (EmployeeID),
  CONSTRAINT CK_Birthdate CHECK (BirthDate < getdate())
)
GO

-- Table Region
CREATE TABLE Region (
  RegionID int NOT NULL,
  RegionDescription char(50)  NOT NULL,
  CONSTRAINT PK_Region PRIMARY KEY (RegionID)
)
GO

-- Table Territories
CREATE TABLE Territories (
  TerritoryID varchar(20)  NOT NULL,
  TerritoryDescription char(50)  NOT NULL,
  RegionID int NOT NULL,
  CONSTRAINT PK_Territories PRIMARY KEY (TerritoryID)
)
GO

-- Table EmployeeTerritories
CREATE TABLE EmployeeTerritories (
  EmployeeID int NOT NULL,
  TerritoryID varchar(20)  NOT NULL
)
GO

-- Table Suppliers
CREATE TABLE Suppliers (
  SupplierID int IDENTITY(1, 1) NOT NULL,
  CompanyName varchar(40)  NOT NULL,
  ContactName varchar(30)  NULL,
  ContactTitle varchar(30)  NULL,
  Address varchar(60)  NULL,
  City varchar(15)  NULL,
  Region varchar(15)  NULL,
  PostalCode varchar(10)  NULL,
  Country varchar(15)  NULL,
  Phone varchar(24)  NULL,
  Fax varchar(24)  NULL,
  HomePage text  NULL,
  CONSTRAINT PK_Suppliers PRIMARY KEY (SupplierID)
)
GO

-- Table Products  
CREATE TABLE Products (
  ProductID int IDENTITY(1, 1) NOT NULL,
  ProductName varchar(40)  NOT NULL,
  SupplierID int NULL,
  CategoryID int NULL,
  QuantityPerUnit varchar(20)  NULL,
  UnitPrice money CONSTRAINT DF_Products_UnitPrice DEFAULT 0 NULL,
  UnitsInStock smallint CONSTRAINT DF_Products_UnitsInStock DEFAULT 0 NULL,
  UnitsOnOrder smallint CONSTRAINT DF_Products_UnitsOnOrder DEFAULT 0 NULL,
  ReorderLevel smallint CONSTRAINT DF_Products_ReorderLevel DEFAULT 0 NULL,
  Discontinued bit CONSTRAINT DF_Products_Discontinued DEFAULT 0 NOT NULL,
  CONSTRAINT PK_Products PRIMARY KEY (ProductID),
  CONSTRAINT CK_Products_UnitPrice CHECK (UnitPrice >=(0)),
  CONSTRAINT CK_ReorderLevel CHECK (ReorderLevel >=(0)),
  CONSTRAINT CK_UnitsInStock CHECK (UnitsInStock >=(0)),
  CONSTRAINT CK_UnitsOnOrder CHECK (UnitsOnOrder >=(0))
)
GO

-- Table Shippers
CREATE TABLE Shippers (
  ShipperID int IDENTITY(1, 1) NOT NULL,
  CompanyName varchar(40)  NOT NULL,
  Phone varchar(24)  NULL,
  CONSTRAINT PK_Shippers PRIMARY KEY (ShipperID)
)
GO

-- Table Orders
CREATE TABLE Orders (
  OrderID int IDENTITY(1, 1) NOT NULL,
  CustomerID char(5)  NULL,
  EmployeeID int NULL,
  OrderDate datetime NULL,
  RequiredDate datetime NULL,
  ShippedDate datetime NULL,
  ShipVia int NULL,
  Freight money CONSTRAINT DF_Orders_Freight DEFAULT 0 NULL,
  ShipName varchar(40)  NULL,
  ShipAddress varchar(60)  NULL,
  ShipCity varchar(15)  NULL,
  ShipRegion varchar(15)  NULL,
  ShipPostalCode varchar(10)  NULL,
  ShipCountry varchar(15)  NULL,
  CONSTRAINT PK_Orders PRIMARY KEY (OrderID)
)
GO

-- Table OrderDetails
CREATE TABLE OrderDetails (
  OrderID int NOT NULL,
  ProductID int NOT NULL,
  UnitPrice money CONSTRAINT DF_Order_Details_UnitPrice DEFAULT 0 NOT NULL,
  Quantity smallint CONSTRAINT DF_Order_Details_Quantity DEFAULT 1 NOT NULL,
  Discount real CONSTRAINT DF_Order_Details_Discount DEFAULT 0 NOT NULL,
  CONSTRAINT CK_Discount CHECK (Discount>=(0) AND Discount<=(1)),
  CONSTRAINT CK_Quantity CHECK (Quantity>(0)),
  CONSTRAINT CK_UnitPrice CHECK (UnitPrice>=(0))
)
GO

-- user-defined function DateOnly
CREATE FUNCTION DateOnly (@InDateTime datetime)
RETURNS varchar(10)
AS
BEGIN
	DECLARE @MyOutput varchar(10)
	SET @MyOutput = CONVERT(varchar(10),@InDateTime,101)
	RETURN @MyOutput
END
GO

-- user-defined function LookByFName
CREATE FUNCTION LookByFName (@FirstLetter Char)
RETURNS TABLE
AS 
  RETURN SELECT * FROM Employees WHERE LEFT(FirstName, 1) = @FirstLetter
GO

-- user-defined function MyRound
CREATE FUNCTION MyRound (@Operand Decimal,@Places INT)
RETURNS Decimal
AS
BEGIN
Declare @x decimal;
Declare @i int;
Declare @ix int; 

Set @x=@Operand*power(10,@Places);
Set @i=@x;

IF((@x-@i)>=0.5)
 BEGIN
 	  SET @ix=1
 END
ELSE
 BEGIN 
	SET @ix=0;
 END
	
Set @x=@i+@ix;
Set @x=@x/power(10,@Places);

Return(@x);
END
GO

-- View Alphabetical_list_of_products 
create view Alphabetical_list_of_products AS
SELECT Products.*, Categories.CategoryName
      FROM Categories INNER JOIN Products ON Categories.CategoryID = Products.CategoryID
      WHERE (((Products.Discontinued)=0))
GO

-- View Product_Sales_for_1997 
CREATE view Product_Sales_for_1997 AS
SELECT Categories.CategoryName, Products.ProductName, 
Sum(CONVERT(money,("OrderDetails".UnitPrice*Quantity*(1-Discount)/100))*100) AS ProductSales
FROM (Categories INNER JOIN Products ON Categories.CategoryID = Products.CategoryID) 
	INNER JOIN (Orders 
		INNER JOIN "OrderDetails" ON Orders.OrderID = "OrderDetails".OrderID) 
	ON Products.ProductID = "OrderDetails".ProductID
WHERE (((Orders.ShippedDate) Between '19970101' And '19971231'))
GROUP BY Categories.CategoryName, Products.ProductName
GO

-- View Category_Sales_for_1997
CREATE view Category_Sales_for_1997 AS
SELECT "Product_Sales_for_1997".CategoryName, Sum("Product_Sales_for_1997".ProductSales) AS CategorySales
FROM "Product_Sales_for_1997"
GROUP BY "Product_Sales_for_1997".CategoryName
GO

-- View Current_Product_List  
create view Current_Product_List AS
SELECT Product_List.ProductID, Product_List.ProductName
FROM Products AS Product_List
WHERE (((Product_List.Discontinued)=0))
--ORDER BY Product_List.ProductName
GO

-- View Customer_and_Suppliers_by_City  
create view Customer_and_Suppliers_by_City AS
SELECT City, CompanyName, ContactName, 'Customers' AS Relationship 
FROM Customers
UNION SELECT City, CompanyName, ContactName, 'Suppliers'
FROM Suppliers
--ORDER BY City, CompanyName
GO

-- View Invoices  
CREATE view Invoices AS
SELECT Orders.ShipName, Orders.ShipAddress, Orders.ShipCity, Orders.ShipRegion, Orders.ShipPostalCode, 
	Orders.ShipCountry, Orders.CustomerID, Customers.CompanyName AS CustomerName, Customers.Address, Customers.City, 
	Customers.Region, Customers.PostalCode, Customers.Country, 
	(FirstName + ' ' + LastName) AS Salesperson, 
	Orders.OrderID, Orders.OrderDate, Orders.RequiredDate, Orders.ShippedDate, Shippers.CompanyName As ShipperName, 
	"OrderDetails".ProductID, Products.ProductName, "OrderDetails".UnitPrice, "OrderDetails".Quantity, 
	"OrderDetails".Discount, 
	(CONVERT(money,("OrderDetails".UnitPrice*Quantity*(1-Discount)/100))*100) AS ExtendedPrice, Orders.Freight
FROM 	Shippers INNER JOIN 
		(Products INNER JOIN 
			(
				(Employees INNER JOIN 
					(Customers INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID) 
				ON Employees.EmployeeID = Orders.EmployeeID) 
			INNER JOIN "OrderDetails" ON Orders.OrderID = "OrderDetails".OrderID) 
		ON Products.ProductID = "OrderDetails".ProductID) 
	ON Shippers.ShipperID = Orders.ShipVia
GO

-- View Order_Details_Extended  
CREATE view Order_Details_Extended AS
SELECT "OrderDetails".OrderID, "OrderDetails".ProductID, Products.ProductName, 
	"OrderDetails".UnitPrice, "OrderDetails".Quantity, "OrderDetails".Discount, 
	(CONVERT(money,("OrderDetails".UnitPrice*Quantity*(1-Discount)/100))*100) AS ExtendedPrice
FROM Products INNER JOIN "OrderDetails" ON Products.ProductID = "OrderDetails".ProductID
--ORDER BY "Order Details".OrderID
GO

-- View Order_Subtotals  
CREATE view Order_Subtotals AS
SELECT "OrderDetails".OrderID, Sum(CONVERT(money,("OrderDetails".UnitPrice*Quantity*(1-Discount)/100))*100) AS Subtotal
FROM "OrderDetails"
GROUP BY "OrderDetails".OrderID
GO

-- View Orders_Qry  
create view Orders_Qry AS
SELECT Orders.OrderID, Orders.CustomerID, Orders.EmployeeID, Orders.OrderDate, Orders.RequiredDate, 
	Orders.ShippedDate, Orders.ShipVia, Orders.Freight, Orders.ShipName, Orders.ShipAddress, Orders.ShipCity, 
	Orders.ShipRegion, Orders.ShipPostalCode, Orders.ShipCountry, 
	Customers.CompanyName, Customers.Address, Customers.City, Customers.Region, Customers.PostalCode, Customers.Country
FROM Customers INNER JOIN Orders ON Customers.CustomerID = Orders.CustomerID
GO

-- Products_Above_Average_Price  
create view Products_Above_Average_Price AS
SELECT Products.ProductName, Products.UnitPrice
FROM Products
WHERE Products.UnitPrice>(SELECT AVG(UnitPrice) From Products)
--ORDER BY Products.UnitPrice DESC
GO

-- View Products_by_Category  
create view Products_by_Category AS
SELECT Categories.CategoryName, Products.ProductName, Products.QuantityPerUnit, Products.UnitsInStock, Products.Discontinued
FROM Categories INNER JOIN Products ON Categories.CategoryID = Products.CategoryID
WHERE Products.Discontinued <> 1
--ORDER BY Categories.CategoryName, Products.ProductName
GO

-- View Quarterly_Orders  
create view Quarterly_Orders AS
SELECT DISTINCT Customers.CustomerID, Customers.CompanyName, Customers.City, Customers.Country
FROM Customers RIGHT JOIN Orders ON Customers.CustomerID = Orders.CustomerID
WHERE Orders.OrderDate BETWEEN '19970101' And '19971231'
GO

-- View Sales_by_Category  
CREATE view Sales_by_Category AS
SELECT Categories.CategoryID, Categories.CategoryName, Products.ProductName, 
	Sum("Order_Details_Extended".ExtendedPrice) AS ProductSales
FROM 	Categories INNER JOIN 
		(Products INNER JOIN 
			(Orders INNER JOIN "Order_Details_Extended" ON Orders.OrderID = "Order_Details_Extended".OrderID) 
		ON Products.ProductID = "Order_Details_Extended".ProductID) 
	ON Categories.CategoryID = Products.CategoryID
WHERE Orders.OrderDate BETWEEN '19970101' And '19971231'
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
--ORDER BY Products.ProductName
GO

-- View Sales_Totals_by_Amount  
CREATE view Sales_Totals_by_Amount AS
SELECT "Order_Subtotals".Subtotal AS SaleAmount, Orders.OrderID, Customers.CompanyName, Orders.ShippedDate
FROM 	Customers INNER JOIN 
		(Orders INNER JOIN "Order_Subtotals" ON Orders.OrderID = "Order_Subtotals".OrderID) 
	ON Customers.CustomerID = Orders.CustomerID
WHERE ("Order_Subtotals".Subtotal >2500) AND (Orders.ShippedDate BETWEEN '19970101' And '19971231')
GO

-- View Summary_of_Sales_by_Quarter
CREATE view Summary_of_Sales_by_Quarter AS
SELECT Orders.ShippedDate, Orders.OrderID, "Order_Subtotals".Subtotal
FROM Orders INNER JOIN "Order_Subtotals" ON Orders.OrderID = "Order_Subtotals".OrderID
WHERE Orders.ShippedDate IS NOT NULL
--ORDER BY Orders.ShippedDate
GO

-- View Summary_of_Sales_by_Year  
CREATE view Summary_of_Sales_by_Year AS
SELECT Orders.ShippedDate, Orders.OrderID, "Order_Subtotals".Subtotal
FROM Orders INNER JOIN "Order_Subtotals" ON Orders.OrderID = "Order_Subtotals".OrderID
WHERE Orders.ShippedDate IS NOT NULL
--ORDER BY Orders.ShippedDate
GO

-- stored procedure CustOrderHist  
CREATE PROCEDURE CustOrderHist @CustomerID char(5)
AS
SELECT ProductName, Total=SUM(Quantity)
FROM Products P, OrderDetails OD, Orders O, Customers C
WHERE C.CustomerID = @CustomerID
AND C.CustomerID = O.CustomerID AND O.OrderID = OD.OrderID AND OD.ProductID = P.ProductID
GROUP BY ProductName
GO

-- stored procedure CustOrdersDetail  
CREATE PROCEDURE CustOrdersDetail @OrderID int
AS
SELECT ProductName,
    UnitPrice=ROUND(Od.UnitPrice, 2),
    Quantity,
    Discount=CONVERT(int, Discount * 100), 
    ExtendedPrice=ROUND(CONVERT(money, Quantity * (1 - Discount) * Od.UnitPrice), 2)
FROM Products P, OrderDetails Od
WHERE Od.ProductID = P.ProductID and Od.OrderID = @OrderID
GO

-- stored procedure CustOrdersOrders  
CREATE PROCEDURE CustOrdersOrders @CustomerID char(5)
AS
SELECT OrderID, 
	OrderDate,
	RequiredDate,
	ShippedDate
FROM Orders
WHERE CustomerID = @CustomerID
ORDER BY OrderID
GO

-- stored procedure Employee_Sales_by_Country  
CREATE procedure Employee_Sales_by_Country 
@Beginning_Date DateTime, @Ending_Date DateTime AS
SELECT Employees.Country, Employees.LastName, Employees.FirstName, Orders.ShippedDate, Orders.OrderID, "Order_Subtotals".Subtotal AS SaleAmount
FROM Employees INNER JOIN 
	(Orders INNER JOIN "Order_Subtotals" ON Orders.OrderID = "Order_Subtotals".OrderID) 
	ON Employees.EmployeeID = Orders.EmployeeID
WHERE Orders.ShippedDate Between @Beginning_Date And @Ending_Date
GO

-- stored procedure Sales_by_Year
CREATE procedure Sales_by_Year 
	@Beginning_Date DateTime, @Ending_Date DateTime AS
SELECT Orders.ShippedDate, Orders.OrderID, "Order_Subtotals".Subtotal, DATENAME(yy,ShippedDate) AS Year
FROM Orders INNER JOIN "Order_Subtotals" ON Orders.OrderID = "Order_Subtotals".OrderID
WHERE Orders.ShippedDate Between @Beginning_Date And @Ending_Date
GO

-- stored procedure SalesByCategory  
CREATE PROCEDURE SalesByCategory
    @CategoryName varchar(15), @OrdYear varchar(4) = '1998'
AS
IF @OrdYear != '1996' AND @OrdYear != '1997' AND @OrdYear != '1998' 
BEGIN
	SELECT @OrdYear = '1998'
END

SELECT ProductName,
	TotalPurchase=ROUND(SUM(CONVERT(decimal(14,2), OD.Quantity * (1-OD.Discount) * OD.UnitPrice)), 0)
FROM OrderDetails OD, Orders O, Products P, Categories C
WHERE OD.OrderID = O.OrderID 
	AND OD.ProductID = P.ProductID 
	AND P.CategoryID = C.CategoryID
	AND C.CategoryName = @CategoryName
	AND SUBSTRING(CONVERT(varchar(22), O.OrderDate, 111), 1, 4) = @OrdYear
GROUP BY ProductName
ORDER BY ProductName
GO

-- stored procedure sp_Employees_Cursor  
CREATE PROCEDURE sp_Employees_Cursor 
	@CityIn varchar(15)
AS
BEGIN
	
Declare @FName as varchar(10)
Declare @LName as varchar(20)
Declare @PhotoPath as varchar(255)

Declare @people TABLE 
( 
    FName varchar(10), 
    LName varchar(20),
    PhotoPath varchar(255) 
)

Declare EmployeeCursor CURSOR FAST_FORWARD FOR

Select FirstName, LastName, PhotoPath from Employees where City = @CityIn order by FirstName

OPEN EmployeeCursor
FETCH NEXT FROM EmployeeCursor
INTO @FName, @LName, @PhotoPath

WHILE @@FETCH_STATUS = 0
BEGIN

     INSERT INTO @people VALUES(@FName,@LName,@PhotoPath)
     
     FETCH NEXT FROM EmployeeCursor
     INTO @FName, @LName, @PhotoPath
END

SELECT * FROM @people

CLOSE EmployeeCursor
DEALLOCATE EmployeeCursor

END
GO

-- stored procedure sp_Employees_Insert
CREATE PROCEDURE sp_Employees_Insert
@LastName varchar,@FirstName varchar,@Title varchar = NULL,@TitleOfCourtesy varchar = NULL,@BirthDate DateTime = NULL,@HireDate DateTime = NULL,@Address varchar = NULL,@City varchar = NULL,@Region varchar = NULL,@PostalCode varchar = NULL,@Country varchar = NULL,@HomePhone varchar = NULL,@Extension varchar = NULL,@Photo Image = NULL,@Notes text = NULL,@ReportsTo Int = NULL,@ReturnID Int OUT
AS
BEGIN
Insert Into Employees(LastName,FirstName,Title,TitleOfCourtesy,BirthDate,HireDate,Address,City,Region,PostalCode,Country,HomePhone,Extension,Photo,Notes,ReportsTo)
	Values(@LastName,@FirstName,@Title,@TitleOfCourtesy,@BirthDate,@HireDate,@Address,@City,@Region,@PostalCode,@Country,@HomePhone,@Extension,@Photo,@Notes,@ReportsTo);

	SELECT @ReturnID = @@IDENTITY;
END
GO

-- stored procedure sp_employees_rank
CREATE PROCEDURE sp_employees_rank
AS
BEGIN

select e.Title, e.EmployeeID, e.FirstName, e.Salary,
RANK() OVER (Partition by e.Title Order By e.Title) AS RANKS
FROM Employees e 

END
GO

-- stored procedure sp_employees_rollup  
CREATE PROCEDURE sp_employees_rollup
AS
BEGIN

SELECT City ,Sum(Salary) Salary_By_City FROM employees
GROUP BY City WITH ROLLUP

END
GO

-- stored procedure sp_employees_rownum  
CREATE PROCEDURE sp_employees_rownum
AS
BEGIN
SELECT * 
FROM
(
SELECT Row_Number() over(order by p.firstname desc) AS ROWNUM,
p.* FROM Employees p
) a
WHERE a.ROWNUM>=2 AND a.ROWNUM<=4
END
GO

-- stored procedure sp_Employees_SelectAll  
CREATE PROCEDURE sp_Employees_SelectAll
AS
BEGIN
Select 
		EmployeeID,
		LastName,
		FirstName,
		Title,
		TitleOfCourtesy,
		BirthDate,
		HireDate,
		Address,
		City,
		Region,
		PostalCode,
		Country,
		HomePhone,
		Extension,
		Photo,
		Notes,
		ReportsTo
	From Employees;
END
GO

-- stored procedure sp_Employees_SelectRow  
CREATE PROCEDURE sp_Employees_SelectRow
@EmployeeID Int
AS
BEGIN
Select 
		EmployeeID,
		LastName,
		FirstName,
		Title,
		TitleOfCourtesy,
		BirthDate,
		HireDate,
		Address,
		City,
		Region,
		PostalCode,
		Country,
		HomePhone,
		Extension,
		Photo,
		Notes,
		ReportsTo
	From Employees
	Where
		EmployeeID = @EmployeeID;
END
GO

-- stored procedure sp_Employees_Update  
CREATE PROCEDURE sp_Employees_Update
@EmployeeID Int,@LastName varchar,@FirstName varchar,@Title varchar,@TitleOfCourtesy varchar,@BirthDate DateTime,@HireDate DateTime,@Address varchar,@City varchar,@Region varchar,@PostalCode varchar,@Country varchar,@HomePhone varchar,@Extension varchar,@Photo Image,@Notes text,@ReportsTo Int
AS
BEGIN
Update Employees
	Set
		LastName = @LastName,
		FirstName = @FirstName,
		Title = @Title,
		TitleOfCourtesy = @TitleOfCourtesy,
		BirthDate = @BirthDate,
		HireDate = @HireDate,
		Address = @Address,
		City = @City,
		Region = @Region,
		PostalCode = @PostalCode,
		Country = @Country,
		HomePhone = @HomePhone,
		Extension = @Extension,
		Photo = @Photo,
		Notes = @Notes,
		ReportsTo = @ReportsTo
	Where		
		EmployeeID = @EmployeeID;
END
GO

-- stored procedure Ten_Most_Expensive_Products
create procedure Ten_Most_Expensive_Products AS
SET ROWCOUNT 10
SELECT Products.ProductName AS TenMostExpensiveProducts, Products.UnitPrice
FROM Products
ORDER BY Products.UnitPrice DESC
GO

--
-- foreign keys  
--

ALTER TABLE CustomerCustomerDemo
ADD CONSTRAINT FK_CustomerCustomerDemo FOREIGN KEY (CustomerTypeID) 
  REFERENCES CustomerDemographics (CustomerTypeID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE CustomerCustomerDemo
ADD CONSTRAINT FK_CustomerCustomerDemo_Customers FOREIGN KEY (CustomerID) 
  REFERENCES Customers (CustomerID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE Employees
ADD CONSTRAINT FK_Employees_Employees FOREIGN KEY (ReportsTo) 
  REFERENCES Employees (EmployeeID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE EmployeeTerritories
ADD CONSTRAINT FK_EmployeeTerritories_Employees FOREIGN KEY (EmployeeID) 
  REFERENCES Employees (EmployeeID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE EmployeeTerritories
ADD CONSTRAINT FK_EmployeeTerritories_Territories FOREIGN KEY (TerritoryID) 
  REFERENCES Territories (TerritoryID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE Products
ADD CONSTRAINT FK_Products_Categories FOREIGN KEY (CategoryID) 
  REFERENCES Categories (CategoryID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE Products
ADD CONSTRAINT FK_Products_Suppliers FOREIGN KEY (SupplierID) 
  REFERENCES Suppliers (SupplierID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE Orders
ADD CONSTRAINT FK_Orders_Customers FOREIGN KEY (CustomerID) 
  REFERENCES Customers (CustomerID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE Orders
ADD CONSTRAINT FK_Orders_Employees FOREIGN KEY (EmployeeID) 
  REFERENCES Employees (EmployeeID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE Orders
ADD CONSTRAINT FK_Orders_Shippers FOREIGN KEY (ShipVia) 
  REFERENCES Shippers (ShipperID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE OrderDetails
ADD CONSTRAINT FK_Order_Details_Orders FOREIGN KEY (OrderID) 
  REFERENCES Orders (OrderID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE OrderDetails
ADD CONSTRAINT FK_Order_Details_Products FOREIGN KEY (ProductID) 
  REFERENCES Products (ProductID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO


ALTER TABLE Territories
ADD CONSTRAINT FK_Territories_Region FOREIGN KEY (RegionID) 
  REFERENCES Region (RegionID) 
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO 
