-- Copyright Microsoft, Inc. 1994 - 2000
-- All Rights Reserved.
--
-- Create Database Northwind SQLITE 3
-- Ported by Luis A. Sierra
-- Blog: https://interesting-homemade-projects.blogspot.com
--
-- TESTED in Database Management ** SQLite Browser **
-- https://sqlitebrowser.org

-- Categories
DROP TABLE IF EXISTS Categories;
CREATE TABLE Categories 
(      
   CategoryID  INTEGER PRIMARY KEY AUTOINCREMENT,
   CategoryName  TEXT,
   Description  TEXT,
   Picture  BLOB
);

-- CustomerCustomerDemo
DROP TABLE IF EXISTS CustomerCustomerDemo;
CREATE TABLE CustomerCustomerDemo(
   CustomerID TEXT NOT NULL,
   CustomerTypeID TEXT NOT NULL,
   PRIMARY KEY ("CustomerID","CustomerTypeID"),
   FOREIGN KEY (CustomerID) REFERENCES Customers  (CustomerID)
   ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY (CustomerTypeID) REFERENCES CustomerDemographics  (CustomerTypeID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- CustomerDemographics
DROP TABLE IF EXISTS CustomerDemographics;
CREATE TABLE CustomerDemographics(
   CustomerTypeID TEXT NOT NULL,
   CustomerDesc TEXT,
   PRIMARY KEY ("CustomerTypeID")
);

-- Customers
DROP TABLE IF EXISTS Customers;
CREATE TABLE Customers 
(      
  CustomerID  TEXT,
  CompanyName  TEXT,
  ContactName  TEXT,
  ContactTitle  TEXT,
  Address  TEXT,
  City  TEXT,
  Region  TEXT,
  PostalCode  TEXT,
  Country  TEXT,
  Phone  TEXT,
  Fax  TEXT,
  PRIMARY KEY (`CustomerID`)
);

-- Employee
DROP TABLE IF EXISTS Employees;
CREATE TABLE Employees 
(      
   EmployeeID  INTEGER PRIMARY KEY AUTOINCREMENT,
   LastName  TEXT,
   FirstName  TEXT,
   Title  TEXT,
   TitleOfCourtesy  TEXT,
   BirthDate  DATE,
   HireDate  DATE,
   Address  TEXT,
   City  TEXT,
   Region  TEXT,
   PostalCode  TEXT,
   Country  TEXT,
   HomePhone  TEXT,
   Extension  TEXT,
   Photo  BLOB,
   Notes  TEXT,
   ReportsTo  INTEGER,
   PhotoPath  TEXT,
   Salary DECIMAL (18,2),
   FOREIGN KEY (EmployeeID) REFERENCES Employees  (EmployeeID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

-- EmployeeTerritories
DROP TABLE IF EXISTS EmployeeTerritories;
CREATE TABLE EmployeeTerritories(
   EmployeeID INTEGER NOT NULL,
   TerritoryID TEXT NOT NULL,
   FOREIGN KEY (EmployeeID) REFERENCES Employees  (EmployeeID) 
   ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY (TerritoryID) REFERENCES Territories (TerritoryID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--Order Details 
DROP TABLE IF EXISTS OrderDetails;
CREATE TABLE OrderDetails(
   OrderID INTEGER NOT NULL,
   ProductID INTEGER NOT NULL,
   UnitPrice NUMERIC NOT NULL DEFAULT 0,
   Quantity INTEGER NOT NULL DEFAULT 1,
   Discount REAL NOT NULL DEFAULT 0,
   CHECK (Discount>=(0) AND Discount<=(1)),
   CHECK (Quantity>(0)),
   CHECK (UnitPrice>=(0)),
   FOREIGN KEY (OrderID) REFERENCES Orders (OrderID) ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY (ProductID) REFERENCES Products (ProductID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--Orders
DROP TABLE IF EXISTS Orders;
CREATE TABLE Orders (
   OrderID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   CustomerID TEXT,
   EmployeeID INTEGER,
   OrderDate DATETIME,
   RequiredDate DATETIME,
   ShippedDate DATETIME,
   ShipVia INTEGER,
   Freight NUMERIC DEFAULT 0,
   ShipName TEXT,
   ShipAddress TEXT,
   ShipCity TEXT,
   ShipRegion TEXT,
   ShipPostalCode TEXT,
   ShipCountry TEXT,
   FOREIGN KEY (EmployeeID) REFERENCES Employees (EmployeeID) ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY (CustomerID) REFERENCES Customers (CustomerID) ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY (ShipVia) REFERENCES Shippers  (ShipperID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--Products
DROP TABLE IF EXISTS Products;
CREATE TABLE Products(
   ProductID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   ProductName TEXT NOT NULL,
   SupplierID INTEGER,
   CategoryID INTEGER,
   QuantityPerUnit TEXT,
   UnitPrice NUMERIC DEFAULT 0,
   UnitsInStock INTEGER DEFAULT 0,
   UnitsOnOrder INTEGER DEFAULT 0,
   ReorderLevel INTEGER DEFAULT 0,
   Discontinued TEXT NOT NULL DEFAULT '0',
   CHECK (UnitPrice >= (0)),
   CHECK (ReorderLevel >= (0)),
   CHECK (UnitsInStock >= (0)),
   CHECK (UnitsOnOrder >= (0)),
   FOREIGN KEY (CategoryID) REFERENCES Categories (CategoryID) ON DELETE NO ACTION ON UPDATE NO ACTION,
   FOREIGN KEY (SupplierID) REFERENCES Suppliers (SupplierID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--Regions
DROP TABLE IF EXISTS Regions;
CREATE TABLE Regions(
   RegionID INTEGER NOT NULL PRIMARY KEY,
   RegionDescription TEXT NOT NULL
);

--Shippers
DROP TABLE IF EXISTS Shippers;
CREATE TABLE Shippers(
   ShipperID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   CompanyName TEXT NOT NULL,
   Phone TEXT
);

--Suppliers
DROP TABLE IF EXISTS Suppliers;
CREATE TABLE Suppliers(
   SupplierID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
   CompanyName TEXT NOT NULL,
   ContactName TEXT,
   ContactTitle TEXT,
   Address TEXT,
   City TEXT,
   Region TEXT,
   PostalCode TEXT,
   Country TEXT,
   Phone TEXT,
   Fax TEXT,
   HomePage TEXT
);

--Territories
DROP TABLE IF EXISTS Territories;
CREATE TABLE Territories(
   TerritoryID TEXT NOT NULL,
   TerritoryDescription TEXT NOT NULL,
   RegionID INTEGER NOT NULL,
   PRIMARY KEY ("TerritoryID"),
   FOREIGN KEY (RegionID) REFERENCES Regions (RegionID) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--Alphabetical list of products 
DROP VIEW IF EXISTS Alphabetical_list_of_products;
--
CREATE VIEW Alphabetical_list_of_products  
AS
SELECT Products.*, 
       Categories.CategoryName
FROM Categories 
   INNER JOIN Products ON Categories.CategoryID = Products.CategoryID
WHERE (((Products.Discontinued)=0));

--Current Product List
DROP VIEW IF EXISTS Current_Product_List;
CREATE VIEW Current_Product_List  
AS
SELECT ProductID,
       ProductName 
FROM Products 
WHERE Discontinued = 0;

--Customer and Suppliers by City
DROP VIEW IF EXISTS Customer_and_Suppliers_by_City;
CREATE VIEW Customer_and_Suppliers_by_City  
AS
SELECT City, 
       CompanyName, 
       ContactName, 
       'Customers' AS Relationship 
FROM Customers
UNION 
SELECT City, 
       CompanyName, 
       ContactName, 
       'Suppliers'
FROM Suppliers 
ORDER BY City, CompanyName;

--Invoices
DROP VIEW IF EXISTS Invoices;
CREATE VIEW Invoices  
AS
SELECT Orders.ShipName,
       Orders.ShipAddress,
       Orders.ShipCity,
       Orders.ShipRegion, 
       Orders.ShipPostalCode,
       Orders.ShipCountry,
       Orders.CustomerID,
       Customers.CompanyName AS CustomerName, 
       Customers.Address,
       Customers.City,
       Customers.Region,
       Customers.PostalCode,
       Customers.Country,
       (Employees.FirstName + ' ' + Employees.LastName) AS Salesperson, 
       Orders.OrderID,
       Orders.OrderDate,
       Orders.RequiredDate,
       Orders.ShippedDate, 
       Shippers.CompanyName As ShipperName,
       OrderDetails.ProductID,
       Products.ProductName, 
       OrderDetails.UnitPrice,
       OrderDetails.Quantity,
       OrderDetails.Discount, 
       (((OrderDetails.UnitPrice * Quantity * (1-Discount))/100)*100) AS ExtendedPrice,
       Orders.Freight 
FROM Customers 
JOIN Orders ON Customers.CustomerID = Orders.CustomerID  
JOIN Employees ON Employees.EmployeeID = Orders.EmployeeID    
JOIN OrderDetails ON Orders.OrderID = OrderDetails.OrderID     
JOIN Products ON Products.ProductID = OrderDetails.ProductID      
JOIN Shippers ON Shippers.ShipperID = Orders.ShipVia;
--
--Orders Qry
DROP VIEW IF EXISTS Orders_Qry;
CREATE VIEW Orders_Qry  AS
SELECT Orders.OrderID,
       Orders.CustomerID,
       Orders.EmployeeID, 
       Orders.OrderDate, 
       Orders.RequiredDate,
       Orders.ShippedDate, 
       Orders.ShipVia, 
       Orders.Freight,
       Orders.ShipName, 
       Orders.ShipAddress, 
       Orders.ShipCity,
       Orders.ShipRegion,
       Orders.ShipPostalCode,
       Orders.ShipCountry,
       Customers.CompanyName,
       Customers.Address,
       Customers.City,
       Customers.Region,
       Customers.PostalCode, 
       Customers.Country
FROM Customers 
     JOIN Orders ON Customers.CustomerID = Orders.CustomerID;     

--Order Subtotals 
DROP VIEW IF EXISTS Order_Subtotals;
CREATE VIEW Order_Subtotals AS
SELECT OrderDetails.OrderID, 
Sum((OrderDetails.UnitPrice * Quantity * (1-Discount)/100) * 100) AS Subtotal
FROM OrderDetails 
GROUP BY OrderDetails.OrderID;

--Product Sales for 1997 
DROP VIEW IF EXISTS Product_Sales_for_1997;
CREATE VIEW Product_Sales_for_1997  AS
SELECT Categories.CategoryName, 
       Products.ProductName, 
       Sum((OrderDetails.UnitPrice * Quantity * (1-Discount)/100) * 100) AS ProductSales
FROM Categories
JOIN Products On Categories.CategoryID = Products.CategoryID
JOIN OrderDetails on Products.ProductID = OrderDetails.ProductID     
JOIN Orders on Orders.OrderID = OrderDetails.OrderID 
WHERE Orders.ShippedDate Between DATETIME('1997-01-01') And DATETIME('1997-12-31')
GROUP BY Categories.CategoryName, Products.ProductName;

--Products Above Average Price 
DROP VIEW IF EXISTS Products_Above_Average_Price;
--
CREATE VIEW Products_Above_Average_Price AS
SELECT Products.ProductName, 
       Products.UnitPrice
FROM Products
WHERE Products.UnitPrice > (SELECT AVG(UnitPrice) From Products);

--Products by Category 
DROP VIEW IF EXISTS Products_by_Category;
CREATE VIEW Products_by_Category AS
SELECT Categories.CategoryName, 
       Products.ProductName, 
       Products.QuantityPerUnit, 
       Products.UnitsInStock, 
       Products.Discontinued
FROM Categories 
     INNER JOIN Products ON Categories.CategoryID = Products.CategoryID
WHERE Products.Discontinued <> 1;

--Quarterly Orders 
DROP VIEW IF EXISTS Quarterly_Orders;
CREATE VIEW Quarterly_Orders  AS
SELECT DISTINCT Customers.CustomerID, 
                Customers.CompanyName, 
                Customers.City, 
                Customers.Country
FROM Customers 
JOIN Orders ON Customers.CustomerID = Orders.CustomerID
WHERE Orders.OrderDate BETWEEN DATETIME('1997-01-01') And DATETIME('1997-12-31');

--Sales Totals by Amount 
DROP VIEW IF EXISTS Sales_Totals_by_Amount;
CREATE VIEW Sales_Totals_by_Amount  AS
SELECT Order_Subtotals.Subtotal AS SaleAmount, 
       Orders.OrderID, 
       Customers.CompanyName, 
       Orders.ShippedDate
FROM Customers 
JOIN Orders ON Customers.CustomerID = Orders.CustomerID
JOIN Order_Subtotals ON Orders.OrderID = Order_Subtotals.OrderID 
WHERE (Order_Subtotals.Subtotal > 2500) 
AND (Orders.ShippedDate BETWEEN DATETIME('1997-01-01') And DATETIME('1997-12-31'));

--Summary of Sales by Quarter 
DROP VIEW IF EXISTS Summary_of_Sales_by_Quarter;
CREATE VIEW Summary_of_Sales_by_Quarter  AS
SELECT Orders.ShippedDate, 
       Orders.OrderID, 
       Order_Subtotals.Subtotal
FROM Orders 
     INNER JOIN Order_Subtotals  ON Orders.OrderID = Order_Subtotals.OrderID
WHERE Orders.ShippedDate IS NOT NULL;

--Summary of Sales by Year 
DROP VIEW IF EXISTS Summary_of_Sales_by_Year;
CREATE VIEW Summary_of_Sales_by_Year AS
SELECT Orders.ShippedDate, 
       Orders.OrderID, 
       Order_Subtotals.Subtotal
FROM Orders 
INNER JOIN Order_Subtotals  ON Orders.OrderID = Order_Subtotals.OrderID
WHERE Orders.ShippedDate IS NOT NULL;

--Category Sales for 1997 
DROP VIEW IF EXISTS Category_Sales_for_1997;
CREATE VIEW Category_Sales_for_1997 AS
SELECT Product_Sales_for_1997.CategoryName, 
       Sum(Product_Sales_for_1997.ProductSales) AS CategorySales
FROM Product_Sales_for_1997
GROUP BY Product_Sales_for_1997.CategoryName;

--Order Details Extended 
DROP VIEW IF EXISTS Order_Details_Extended;
CREATE VIEW Order_Details_Extended  AS
SELECT OrderDetails.OrderID, 
       OrderDetails.ProductID, 
       Products.ProductName, 
	   OrderDetails.UnitPrice, 
       OrderDetails.Quantity, 
       OrderDetails.Discount, 
      (OrderDetails.UnitPrice * Quantity * (1-Discount)/100) * 100 AS ExtendedPrice
FROM Products 
JOIN OrderDetails ON Products.ProductID = OrderDetails.ProductID;     

--Sales by Category 
DROP VIEW IF EXISTS Sales_by_Category;
--
CREATE VIEW Sales_by_Category AS
SELECT Categories.CategoryID, 
       Categories.CategoryName, 
       Products.ProductName, 
	   Sum(Order_Details_Extended.ExtendedPrice) AS ProductSales
FROM  Categories 
JOIN Products 
ON Categories.CategoryID = Products.CategoryID
JOIN Order_Details_Extended ON Products.ProductID = Order_Details_Extended.ProductID                
JOIN Orders ON Orders.OrderID = Order_Details_Extended.OrderID 
WHERE Orders.OrderDate BETWEEN DATETIME('1997-01-01') And DATETIME('1997-12-31')
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName;
--OK