-- Copyright Microsoft, Inc. 1994 - 2000
-- All Rights Reserved.
--
-- Create Database Northwind Oracle Database 10g
-- Created and Ported by Luis A. Sierra
-- Blog: https://interesting-homemade-projects.blogspot.com
--
-- TESTED in Database Management ** Oracle SQL Developer and Oracle Database 10g **
-- https://www.oracle.com/database/sqldeveloper/technologies/download/
-- https://www.oracle.com/database/technologies/xe-downloads.html

-- SID: (xe) For Express
-- SID: (orcl) For Enterprise
--
-- Login to the default database (SID) with the SYSTEM user
--
-- Create a USER: NORTHWIND with PASSWORD and Login to the SCHEMA Northwind.

-- Categories
CREATE TABLE NORTHWIND.Categories
(
  CategoryID NUMBER NOT NULL PRIMARY KEY,
  CategoryName VARCHAR2(15),
  Description VARCHAR2(500),
  Picture BLOB
);

-- Auto-increment for Table Categories
CREATE SEQUENCE NORTHWIND.SEQ_CATEGORIES  
    MINVALUE 1
    MAXVALUE 999999999999999999999999999
    START WITH 1
    INCREMENT BY 1
    NOCYCLE
    NOCACHE
    NOORDER;
	
-- CustomerDemographics
CREATE TABLE NORTHWIND.CustomerDemographics 
(
   CustomerTypeID VARCHAR2(10) NOT NULL PRIMARY KEY,
   CustomerDesc VARCHAR2(500)
);
	
-- CustomerCustomerDemo
CREATE TABLE NORTHWIND.CustomerCustomerDemo
(
   CustomerID VARCHAR2(5) NOT NULL PRIMARY KEY,
   CustomerTypeID VARCHAR2(10) NOT NULL
);

-- Customers
CREATE TABLE NORTHWIND.Customers
(        	   
	CustomerID VARCHAR2(5) NOT NULL PRIMARY KEY,
	CompanyName VARCHAR2(40) NOT NULL,
	ContactName VARCHAR2(30),
	ContactTitle VARCHAR2(30),
	Address VARCHAR2(60),
	City VARCHAR2(15),
	Region VARCHAR2(15),
	PostalCode VARCHAR2(10),
	Country VARCHAR2(15),
	Phone VARCHAR2(24),
	Fax VARCHAR2(24)
);

-- Employee
CREATE TABLE NORTHWIND.Employees
(      
   EmployeeID NUMBER NOT NULL PRIMARY KEY,
   LastName VARCHAR2(20),
   FirstName VARCHAR2(10),
   Title VARCHAR2(30),
   TitleOfCourtesy VARCHAR2(25),
   BirthDate DATE,
   HireDate DATE,
   Address VARCHAR2(60),
   City VARCHAR2(15),
   Region VARCHAR2(15),
   PostalCode VARCHAR2(10),
   Country VARCHAR2(15),
   HomePhone VARCHAR2(24),
   Extension VARCHAR2(4),
   Photo BLOB,
   Notes VARCHAR2(1000),
   ReportsTo NUMBER,
   PhotoPath VARCHAR2(3000),
   Salary FLOAT
);

-- Auto-increment for Table Employees
CREATE SEQUENCE NORTHWIND.SEQ_EMPLOYEES 
    MINVALUE 1
    MAXVALUE 999999999999999999999999999
    START WITH 1
    INCREMENT BY 1
    NOCYCLE
    NOCACHE
    NOORDER;
	
-- Regions
CREATE TABLE NORTHWIND.Regions
(
   RegionID NUMBER NOT NULL PRIMARY KEY,
   RegionDescription VARCHAR2(50) NOT NULL
);

-- Territories
CREATE TABLE NORTHWIND.Territories
(
   TerritoryID VARCHAR2(20) NOT NULL PRIMARY KEY,
   TerritoryDescription VARCHAR2(50) NOT NULL,
   RegionID NUMBER NOT NULL
);

-- EmployeeTerritories
CREATE TABLE NORTHWIND.EmployeeTerritories
(
   EmployeeID NUMBER NOT NULL,
   TerritoryID VARCHAR2(20) NOT NULL
);

-- Suppliers
CREATE TABLE NORTHWIND.Suppliers
(
   SupplierID NUMBER NOT NULL PRIMARY KEY,
   CompanyName VARCHAR2(40) NOT NULL,
   ContactName VARCHAR2(30),
   ContactTitle VARCHAR2(30),
   Address VARCHAR2(60),
   City VARCHAR2(15),
   Region VARCHAR2(15),
   PostalCode VARCHAR2(10),
   Country VARCHAR2(15),
   Phone VARCHAR2(24),
   Fax VARCHAR2(24),
   HomePage VARCHAR2(1000)
);

-- Auto-increment for Table Suppliers
CREATE SEQUENCE NORTHWIND.SEQ_SUPPLIERS
    MINVALUE 1
    MAXVALUE 999999999999999999999999999
    START WITH 1
    INCREMENT BY 1
    NOCYCLE
    NOCACHE
    NOORDER;	
	
-- Shippers
CREATE TABLE NORTHWIND.Shippers 
(
    ShipperID NUMBER NOT NULL PRIMARY KEY,
    CompanyName VARCHAR2(40) NOT NULL,
    Phone VARCHAR2(24)
);
	
-- Auto-increment for Table Shippers
CREATE SEQUENCE NORTHWIND.SEQ_SHIPPERS
    MINVALUE 1
    MAXVALUE 999999999999999999999999999
    START WITH 1
    INCREMENT BY 1
    NOCYCLE
    NOCACHE
    NOORDER;	
	
-- Orders
CREATE TABLE NORTHWIND.Orders (
   OrderID NUMBER NOT NULL PRIMARY KEY,
   CustomerID VARCHAR2(5),
   EmployeeID NUMBER,
   OrderDate DATE,
   RequiredDate DATE,
   ShippedDate DATE,
   ShipVia NUMBER,
   Freight NUMBER DEFAULT 0,
   ShipName VARCHAR2(40),
   ShipAddress VARCHAR2(60),
   ShipCity VARCHAR2(15),
   ShipRegion VARCHAR2(15),
   ShipPostalCode VARCHAR2(10),
   ShipCountry VARCHAR2(15) 
);

-- Auto-increment for Table Orders
CREATE SEQUENCE NORTHWIND.SEQ_ORDERS
    MINVALUE 1
    MAXVALUE 999999999999999999999999999
    START WITH 1
    INCREMENT BY 1
    NOCYCLE
    NOCACHE
    NOORDER;		
	
-- Products
CREATE TABLE NORTHWIND.Products (
    ProductID NUMBER NOT NULL PRIMARY KEY,
    ProductName VARCHAR2(40) NOT NULL,
    SupplierID NUMBER,
    CategoryID NUMBER,
    QuantityPerUnit VARCHAR2(20),
    UnitPrice NUMERIC DEFAULT 0,
    UnitsInStock NUMBER DEFAULT 0,
    UnitsOnOrder NUMBER DEFAULT 0,
    ReorderLevel NUMBER DEFAULT 0,
    Discontinued NUMBER DEFAULT 0,
    CHECK (UnitPrice>=(0)),
    CHECK (ReorderLevel >=(0)),
    CHECK (UnitsInStock>=(0)),
    CHECK (UnitsOnOrder>=(0))
);

-- Auto-increment for Table Products
CREATE SEQUENCE NORTHWIND.SEQ_PRODUCTS
    MINVALUE 1
    MAXVALUE 999999999999999999999999999
    START WITH 1
    INCREMENT BY 1
    NOCYCLE
    NOCACHE
    NOORDER;	

-- OrderDetails
CREATE TABLE NORTHWIND.OrderDetails
(
    OrderID NUMBER NOT NULL,
    ProductID NUMBER NOT NULL,
    UnitPrice NUMBER DEFAULT 0,
    Quantity NUMBER DEFAULT 1,
    Discount FLOAT DEFAULT 0,
    CHECK (Discount>=(0) AND  Discount<=(1)),
    CHECK (Quantity>(0)),
    CHECK (UnitPrice>=(0))
);
	
-- Alphabetical list of products
CREATE VIEW NORTHWIND.Alphabetical_list_of_products
AS
SELECT NORTHWIND.Products.*, 
       NORTHWIND.Categories.CategoryName
FROM NORTHWIND.Categories 
   INNER JOIN NORTHWIND.Products ON NORTHWIND.Categories.CategoryID = NORTHWIND.Products.CategoryID
WHERE (((NORTHWIND.Products.Discontinued)=0));

-- Current Product List
CREATE VIEW NORTHWIND.Current_Product_List
AS
SELECT ProductID,
       ProductName 
FROM NORTHWIND.Products 
WHERE Discontinued=0;
	
-- Customer and Suppliers by City
CREATE VIEW NORTHWIND.Customer_and_Suppliers_by_City
AS
SELECT City, 
       CompanyName, 
       ContactName, 
       'Customers' AS Relationship 
FROM NORTHWIND.Customers
UNION 
SELECT City, 
       CompanyName, 
       ContactName, 
       'Suppliers'
FROM NORTHWIND.Suppliers;
--ORDER BY City,CompanyName;

-- Invoices;
CREATE VIEW NORTHWIND.Invoices  
AS
SELECT NORTHWIND.Orders.ShipName,
       NORTHWIND.Orders.ShipAddress,
       NORTHWIND.Orders.ShipCity,
       NORTHWIND.Orders.ShipRegion, 
       NORTHWIND.Orders.ShipPostalCode,
       NORTHWIND.Orders.ShipCountry,
       NORTHWIND.Orders.CustomerID,
       NORTHWIND.Customers.CompanyName AS CustomerName, 
       NORTHWIND.Customers.Address,
       NORTHWIND.Customers.City,
       NORTHWIND.Customers.Region,
       NORTHWIND.Customers.PostalCode,
       NORTHWIND.Customers.Country,
       (NORTHWIND.Employees.FirstName || ' ' || NORTHWIND.Employees.LastName) AS Salesperson, 
       NORTHWIND.Orders.OrderID,
       NORTHWIND.Orders.OrderDate,
       NORTHWIND.Orders.RequiredDate,
       NORTHWIND.Orders.ShippedDate, 
       NORTHWIND.Shippers.CompanyName As ShipperName,
        NORTHWIND.OrderDetails.ProductID,
       NORTHWIND.Products.ProductName, 
        NORTHWIND.OrderDetails.UnitPrice,
        NORTHWIND.OrderDetails.Quantity,
        NORTHWIND.OrderDetails.Discount, 
       (((NORTHWIND.OrderDetails.UnitPrice * NORTHWIND.OrderDetails.Quantity * (1 - NORTHWIND.OrderDetails.Discount))/100)*100) AS ExtendedPrice,
       NORTHWIND.Orders.Freight 
FROM NORTHWIND.Customers 
  JOIN NORTHWIND.Orders ON NORTHWIND.Customers.CustomerID = NORTHWIND.Orders.CustomerID  
    JOIN NORTHWIND.Employees ON NORTHWIND.Employees.EmployeeID = NORTHWIND.Orders.EmployeeID    
     JOIN  NORTHWIND.OrderDetails  ON NORTHWIND.Orders.OrderID =  NORTHWIND.OrderDetails.OrderID     
      JOIN NORTHWIND.Products ON NORTHWIND.Products.ProductID =  NORTHWIND.OrderDetails.ProductID      
       JOIN NORTHWIND.Shippers ON NORTHWIND.Shippers.ShipperID = NORTHWIND.Orders.ShipVia;

-- Orders Qry;
CREATE VIEW NORTHWIND.Orders_Qry AS
SELECT NORTHWIND.Orders.OrderID,
       NORTHWIND.Orders.CustomerID,
       NORTHWIND.Orders.EmployeeID, 
       NORTHWIND.Orders.OrderDate, 
       NORTHWIND.Orders.RequiredDate,
       NORTHWIND.Orders.ShippedDate, 
       NORTHWIND.Orders.ShipVia, 
       NORTHWIND.Orders.Freight,
       NORTHWIND.Orders.ShipName, 
       NORTHWIND.Orders.ShipAddress, 
       NORTHWIND.Orders.ShipCity,
       NORTHWIND.Orders.ShipRegion,
       NORTHWIND.Orders.ShipPostalCode,
       NORTHWIND.Orders.ShipCountry,
       NORTHWIND.Customers.CompanyName,
       NORTHWIND.Customers.Address,
       NORTHWIND.Customers.City,
       NORTHWIND.Customers.Region,
       NORTHWIND.Customers.PostalCode, 
       NORTHWIND.Customers.Country
FROM NORTHWIND.Customers 
     JOIN NORTHWIND.Orders ON NORTHWIND.Customers.CustomerID = NORTHWIND.Orders.CustomerID;

-- Order Subtotals
CREATE VIEW NORTHWIND.Order_Subtotals AS
SELECT NORTHWIND.OrderDetails.OrderID, 
Sum((NORTHWIND.OrderDetails.UnitPrice * NORTHWIND.OrderDetails.Quantity * (1 - NORTHWIND.OrderDetails.Discount)/100)*100) AS Subtotal
FROM NORTHWIND.OrderDetails
GROUP BY NORTHWIND.OrderDetails.OrderID;

-- Product Sales for 1997
CREATE VIEW NORTHWIND.Product_Sales_for_1997 AS
SELECT NORTHWIND.Categories.CategoryName, 
       NORTHWIND.Products.ProductName, 
       Sum((NORTHWIND.OrderDetails.UnitPrice * NORTHWIND.OrderDetails.Quantity * (1 - NORTHWIND.OrderDetails.Discount)/100)*100) AS ProductSales
FROM NORTHWIND.Categories
 JOIN  NORTHWIND.Products On NORTHWIND.Categories.CategoryID = NORTHWIND.Products.CategoryID
    JOIN  NORTHWIND.OrderDetails on NORTHWIND.Products.ProductID = NORTHWIND.OrderDetails.ProductID     
     JOIN  NORTHWIND.Orders on NORTHWIND.Orders.OrderID = NORTHWIND.OrderDetails.OrderID 
WHERE NORTHWIND.Orders.ShippedDate Between TO_DATE('01/01/1997', 'DD/MM/YYYY') And TO_DATE('31/12/1997', 'DD/MM/YYYY')
GROUP BY NORTHWIND.Categories.CategoryName, NORTHWIND.Products.ProductName;

-- Products Above Average Price
CREATE VIEW NORTHWIND.Products_Above_Average_Price AS
SELECT NORTHWIND.Products.ProductName, 
       NORTHWIND.Products.UnitPrice
FROM NORTHWIND.Products
WHERE NORTHWIND.Products.UnitPrice > (SELECT AVG(UnitPrice) From NORTHWIND.Products);

-- Products by Category
CREATE VIEW NORTHWIND.Products_by_Category AS
SELECT NORTHWIND.Categories.CategoryName, 
       NORTHWIND.Products.ProductName, 
       NORTHWIND.Products.QuantityPerUnit, 
       NORTHWIND.Products.UnitsInStock, 
       NORTHWIND.Products.Discontinued
FROM NORTHWIND.Categories 
     INNER JOIN NORTHWIND.Products ON NORTHWIND.Categories.CategoryID = NORTHWIND.Products.CategoryID
WHERE NORTHWIND.Products.Discontinued <> 1;

-- Quarterly Orders
CREATE VIEW NORTHWIND.Quarterly_Orders AS
SELECT DISTINCT NORTHWIND.Customers.CustomerID, 
                NORTHWIND.Customers.CompanyName, 
                NORTHWIND.Customers.City, 
                NORTHWIND.Customers.Country
FROM NORTHWIND.Customers 
     JOIN NORTHWIND.Orders ON NORTHWIND.Customers.CustomerID = NORTHWIND.Orders.CustomerID
WHERE NORTHWIND.Orders.OrderDate BETWEEN TO_DATE('01/01/1997', 'DD/MM/YYYY') And TO_DATE('31/12/1997', 'DD/MM/YYYY');

-- Sales_Totals_by_Amount
CREATE VIEW NORTHWIND.Sales_Totals_by_Amount AS
SELECT NORTHWIND.Order_Subtotals.Subtotal AS SaleAmount, 
               NORTHWIND.Orders.OrderID, 
               NORTHWIND.Customers.CompanyName, 
               NORTHWIND.Orders.ShippedDate
FROM NORTHWIND.Customers 
 JOIN NORTHWIND.Orders ON NORTHWIND.Customers.CustomerID = NORTHWIND.Orders.CustomerID
    JOIN NORTHWIND.Order_Subtotals ON NORTHWIND.Orders.OrderID = NORTHWIND.Order_Subtotals.OrderID 
WHERE (NORTHWIND.Order_Subtotals.Subtotal > 2500) 
AND (NORTHWIND.Orders.ShippedDate BETWEEN TO_DATE('01/01/1997', 'DD/MM/YYYY') And TO_DATE('31/12/1997', 'DD/MM/YYYY'));

-- Summary of Sales by Quarter
CREATE VIEW NORTHWIND.Summary_of_Sales_by_Quarter AS
SELECT NORTHWIND.Orders.ShippedDate, 
       NORTHWIND.Orders.OrderID, 
       NORTHWIND.Order_Subtotals.Subtotal
FROM NORTHWIND.Orders 
     INNER JOIN NORTHWIND.Order_Subtotals ON NORTHWIND.Orders.OrderID = NORTHWIND.Order_Subtotals.OrderID
WHERE NORTHWIND.Orders.ShippedDate IS NOT NULL;

-- Summary of Sales by Year
CREATE VIEW NORTHWIND.Summary_of_Sales_by_Year AS
SELECT      NORTHWIND.Orders.ShippedDate, 
            NORTHWIND.Orders.OrderID, 
 NORTHWIND.Order_Subtotals.Subtotal
FROM NORTHWIND.Orders 
     INNER JOIN NORTHWIND.Order_Subtotals ON NORTHWIND.Orders.OrderID = NORTHWIND.Order_Subtotals.OrderID
WHERE NORTHWIND.Orders.ShippedDate IS NOT NULL;

-- Category Sales for 1997
CREATE VIEW NORTHWIND.Category_Sales_for_1997 AS
SELECT NORTHWIND.Product_Sales_for_1997.CategoryName, 
       Sum(NORTHWIND.Product_Sales_for_1997.ProductSales) AS CategorySales
FROM NORTHWIND.Product_Sales_for_1997
GROUP BY NORTHWIND.Product_Sales_for_1997.CategoryName;

-- Order Details Extended 
CREATE VIEW NORTHWIND.Order_Details_Extended  AS
SELECT NORTHWIND.OrderDetails.OrderID, 
       NORTHWIND.OrderDetails.ProductID, 
       NORTHWIND.Products.ProductName, 
	   NORTHWIND.OrderDetails.UnitPrice, 
       NORTHWIND.OrderDetails.Quantity, 
       NORTHWIND.OrderDetails.Discount, 
      (NORTHWIND.OrderDetails.UnitPrice * NORTHWIND.OrderDetails.Quantity * (1 - NORTHWIND.OrderDetails.Discount)/100)*100 AS ExtendedPrice
FROM NORTHWIND.Products 
     JOIN NORTHWIND.OrderDetails  ON NORTHWIND.Products.ProductID = NORTHWIND.OrderDetails.ProductID;
	 
-- Sales by Category
CREATE VIEW NORTHWIND.Sales_by_Category AS
SELECT NORTHWIND.Categories.CategoryID, 
       NORTHWIND.Categories.CategoryName, 
       NORTHWIND.Products.ProductName,
	Sum(Order_Details_Extended.ExtendedPrice) AS ProductSales
FROM NORTHWIND.Categories 
    JOIN NORTHWIND.Products 
      ON NORTHWIND.Categories.CategoryID = NORTHWIND.Products.CategoryID
       JOIN NORTHWIND.Order_Details_Extended
         ON NORTHWIND.Products.ProductID = NORTHWIND.Order_Details_Extended.ProductID                
           JOIN NORTHWIND.Orders 
             ON NORTHWIND.Orders.OrderID = NORTHWIND.Order_Details_Extended.OrderID 
WHERE NORTHWIND.Orders.OrderDate BETWEEN TO_DATE('01/01/1997', 'DD/MM/YYYY') And TO_DATE('31/12/1997', 'DD/MM/YYYY')
GROUP BY NORTHWIND.Categories.CategoryID, NORTHWIND.Categories.CategoryName, NORTHWIND.Products.ProductName;

-- CONVERT BFILE TO BLOB
CREATE FUNCTION NORTHWIND.BFILE_TO_BLOB(b_filename IN bfile)
RETURN  blob
IS
  bf bfile := b_filename;
  b blob;
BEGIN
  dbms_lob.createtemporary(b,true);
  dbms_lob.fileopen(bf, dbms_lob.file_readonly);
  dbms_lob.loadfromfile(b,bf,dbms_lob.getlength(bf));
  dbms_lob.fileclose(bf);
  RETURN b ;
END;

--SP_EMPLOYEES_INSERT
CREATE PROCEDURE NORTHWIND.SP_EMPLOYEES_INSERT
(
  p_lastname IN VARCHAR2,
  p_firstname IN VARCHAR2,
  p_title IN VARCHAR2,
  p_titleofcourtesy IN VARCHAR2,
  p_birthdate IN DATE,
  p_hiredate IN DATE,
  p_address IN VARCHAR2,
  p_city IN VARCHAR2,
  p_region IN VARCHAR2,
  p_postalcode IN VARCHAR2,
  p_country IN VARCHAR2,
  p_homephone IN VARCHAR2,
  p_extension  IN VARCHAR2,
  p_photo IN BLOB,
  p_notes IN VARCHAR2,
  p_reportsto IN NUMBER,
  p_photopath IN VARCHAR2,
  p_salary           FLOAT,
  p_employeeid OUT NUMBER 
)
IS
BEGIN

INSERT INTO NORTHWIND.employees (
    EmployeeID,
    LastName,
	FirstName,
	Title,
	TitleOfCourtesy,
	BirthDate,
	HireDate,
	Address,
	City,
	Region,
	PostalCode,
	Country,
	HomePhone,
	Extension,
	Photo,
	Notes,
	ReportsTo,
	PhotoPath,
	Salary)
VALUES (
    NORTHWIND.SEQ_EMPLOYEES.NEXTVAL,
    p_LastName,
	p_FirstName,
	p_Title,
	p_TitleOfCourtesy,
	p_BirthDate,
	p_HireDate,
	p_Address,
	p_City,
	p_Region,
	p_PostalCode,
	p_Country,
	p_HomePhone,
	p_Extension,
	p_Photo,
	p_Notes,
	p_ReportsTo,
	p_PhotoPath,
	p_Salary)
	
    RETURNING EmployeeID INTO p_employeeid;
	--dbms_output.put_line(p_employeeid);
END;

--SP_EMPLOYEES_UPDATE
CREATE PROCEDURE NORTHWIND.SP_EMPLOYEES_UPDATE
(
  p_lastname IN VARCHAR2,
  p_firstname IN VARCHAR2,
  p_title IN VARCHAR2,
  p_titleofcourtesy IN VARCHAR2,
  p_birthdate IN DATE,
  p_hiredate IN DATE,
  p_address IN VARCHAR2,
  p_city IN VARCHAR2,
  p_region IN VARCHAR2,
  p_postalcode IN VARCHAR2,
  p_country IN VARCHAR2,
  p_homephone IN VARCHAR2,
  p_extension  IN VARCHAR2,
  p_photo IN BLOB,
  p_notes IN VARCHAR2,
  p_reportsto IN NUMBER,
  p_photopath IN VARCHAR2,
  p_salary           FLOAT,
  p_employeeid IN NUMBER 
)
IS
BEGIN
  UPDATE NORTHWIND.employees SET
		LastName = p_lastname,
		FirstName = p_firstname,
		Title = p_title,
		TitleOfCourtesy = p_titleofcourtesy,
		BirthDate = p_birthdate,
		HireDate = p_hiredate,
		Address = p_address,
		City = p_city,
		Region = p_region,
		PostalCode = p_postalcode,
		Country = p_country,
		HomePhone = p_homephone,
		Extension = p_extension,
		Photo = p_photo,
		Notes = p_notes,
		ReportsTo = p_reportsto,
		PhotoPath = p_photopath,
		Salary = p_salary
	  WHERE 
		EmployeeID = p_employeeid;
END;

-- MyRound
CREATE FUNCTION NORTHWIND.MyRound (Operand IN FLOAT,Places IN NUMBER)
RETURN FLOAT 
IS
x FLOAT;
i NUMBER;
ix NUMBER;
BEGIN
  x := Operand * POWER(10,Places);
  i := x;
  
  IF (x - i) >= 0.5 THEN                   
     ix := 1;                  
  ELSE
     ix := 0;                 
  END IF;  
  
  x := i + ix;
  x := x / POWER(10,Places);

  RETURN x;
END;

COMMIT WORK;